package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"runtime"
	"time"

	"github.com/nats-io/nats.go"
	"golang.org/x/text/language"
	"golang.org/x/text/message"
)

var p = message.NewPrinter(language.English)

type Offer struct {
	Id     uint64  `json:"id"`
	Profit uint64  `json:"profit"`
	BuyIn  uint64  `json:"buy_in"`
	Risk   float64 `json:"risk"`
}

type Request struct {
	Id uint64 `json:"id"`
}

type Resp struct {
	Type string `json:"type"`
	Id   uint64 `json:"id"`
	Cut  uint64 `json:"cut"`
	Deal Deal   `json:"deal"`
}

type Deal struct {
	Name             string `json:"name"`
	EnchantmentLevel uint32 `json:"enchantment_level"`
	BmQuality        string `json:"bm_quality"`
	CaerQuality      string `json:"caer_quality"`
	BmPrice          uint64 `json:"bm_price"`
	CaerPrice        uint64 `json:"caer_price"`
}

func usage() {
	p.Printf("Usage: blackmarket -s server -u user -p password\n")
	flag.PrintDefaults()
}

func showUsageAndExit(exitcode int) {
	usage()
	os.Exit(exitcode)
}

func printDeal(m *nats.Msg) {
	var deal Offer
	if err := json.Unmarshal(m.Data, &deal); err != nil {
		p.Printf("error unmarshalling deal: %v\n", err)
		return
	}
	p.Printf("%#d:\n\tProfit: %d\n\tBuy-in: %d\n\tRisk: %.2f\n", deal.Id, deal.Profit, deal.BuyIn, deal.Risk)
}

func printResp(msg *nats.Msg) {
	var resp Resp
	err := json.Unmarshal(msg.Data, &resp)
	if err != nil {
		p.Println("failed to deserialize response: ", err)
		return
	}
	switch resp.Type {
	case "Invalid":
		p.Println("Invalid deal id.")
	case "Accepted":
		p.Printf("Accepted %#d. Details:\n\tItem: %s\n\tEnchant: %d\n\tBM Quality: %s\n\tCaer Quality: %s\n\tBM Price: %d\n\tCaer Price: %d\n\tDealer Cut: %d\n",
			resp.Id, resp.Deal.Name, resp.Deal.EnchantmentLevel, resp.Deal.BmQuality, resp.Deal.CaerQuality, resp.Deal.BmPrice, resp.Deal.CaerPrice, resp.Cut)
	default:
		p.Println("Uhh, shouldn't get here.")
	}

}

func main() {
	var help = flag.Bool("h", false, "Show this help message")
	var server = flag.String("s", "localhost:4222", "The nats server URL")
	var user = flag.String("u", "user", "Your nats user")
	var password = flag.String("p", "password", "Your nats password")

	log.SetFlags(0)
	flag.Usage = usage
	flag.Parse()

	if *help {
		showUsageAndExit(0)
	}

	// Connect Options.
	opts := []nats.Option{nats.Name("NATS Sample Subscriber")}
	opts = setupConnOptions(opts)

	url := fmt.Sprintf("nats://%s:%s@%s", *user, *password, *server)
	// Connect to NATS
	nc, err := nats.Connect(url)
	if err != nil {
		log.Fatal(err)
	}

	nc.Subscribe("blackmarket.deals", func(msg *nats.Msg) {
		printDeal(msg)
	})
	nc.Flush()

	if err := nc.LastError(); err != nil {
		log.Fatal(err)
	}

	for {
		var id uint64
		n, err := fmt.Scanln(&id)
		if err != nil {
			if err.Error() == "unexpected newline" {
				continue
			}
			p.Println("invalid input: ", err)
		}
		if n == 0 {
			continue
		}

		req, err := json.Marshal(Request{
			Id: id,
		})
		if err != nil {
			p.Println("failed to build request: ", err)
			continue
		}
		subj := fmt.Sprintf("blackmarket.request.%s", *user)
		msg, err := nc.Request(subj, req, 3*time.Second)
		if err != nil {
			p.Println("failed to make request: ", err)
			continue
		}
		printResp(msg)
	}

	runtime.Goexit()
}

func setupConnOptions(opts []nats.Option) []nats.Option {
	totalWait := 10 * time.Minute
	reconnectDelay := time.Second

	opts = append(opts, nats.ReconnectWait(reconnectDelay))
	opts = append(opts, nats.MaxReconnects(int(totalWait/reconnectDelay)))
	opts = append(opts, nats.DisconnectErrHandler(func(nc *nats.Conn, err error) {
		p.Printf("Disconnected due to:%s, will attempt reconnects for %.0fm\n", err, totalWait.Minutes())
	}))
	opts = append(opts, nats.ReconnectHandler(func(nc *nats.Conn) {
		p.Printf("Reconnected [%s]\n", nc.ConnectedUrl())
	}))
	opts = append(opts, nats.ClosedHandler(func(nc *nats.Conn) {
		log.Fatalf("Exiting: %v", nc.LastError())
	}))
	return opts
}
