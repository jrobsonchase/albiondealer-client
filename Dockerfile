FROM golang

COPY main.go /go/src/albiondealer-client/main.go
WORKDIR /go/src/albiondealer-client

RUN go get

ENTRYPOINT ["/go/bin/albiondealer-client"]
